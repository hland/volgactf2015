#!/usr/bin/python

import sys, os
import requests

# 0 = Size 5152
# 1 = Size 4228
# 2 = Size 3988
# 3 = Size 3650
# 4 = Size 4730
# 5 = Size 5950
# 6 = Size 3998
# 7 = Size 3748
# 8 = Size 3334
# 9 = Size 4476


t = {
    '5152': 0,
    '4228': 1,
    '3988': 2,
    '3650': 3,
    '4730': 4,
    '5950': 5,
    '3998': 6,
    '3748': 7,
    '3334': 8,
    '4476': 9,

# Extra data
    '4832': 5,
    '3236': 8,
    '4474': 9,
    '3996': 6,
    '3498': 4,
    '4036': 6,
    '4614': 7,
    '5602': 9,
    '3320': 8,
}

folder = sys.argv[1]
x = int(sys.argv[2])

print "\n"

allFiles = os.listdir(folder)
allFiles.sort()

files = [fn for fn in allFiles if "captcha%i00" % (x) in fn]
print "Solving captcha#%i" % (x)
captcha = ""
for fn in files:
    if fn.endswith(".wav"):
        print fn
        path = os.path.join(folder, fn)
        size = str(os.path.getsize(path))
        #print "%s %s" % (path, size)
        if t.has_key(size):
           captcha += str(t[size])
        else:
            captcha += "?"

captcha = captcha[0:6]
print "%s\n" % (captcha)
print "Submitting captcha.. %s" % (captcha)

cookie = ""

with open(os.path.join(folder, "cookies.txt"), "r") as f:
    cookie = f.read().split('\t')[-1].replace('\n', '')
    print "Sending cookie: %s" % (cookie)

payload = {'captcha': captcha}
cookies = dict(JSESSIONID=cookie)
response = requests.post('http://yacst.2015.volgactf.ru/captcha', payload, cookies=cookies)
print response.text

