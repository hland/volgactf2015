# README

Solution to YACST - 200 point challenge

# Howto

To solve captchas just run: ./solve.sh
This will create a new timestamped directory, pull down a captcha .wav file, split it into parts and using the size of the split files to solve the challenge. 

Working on Kali Linux x64 (Linux kali 3.14-kali1-amd64 #1 SMP Debian 3.14.5-1kali1 (2014-06-07) x86_64 GNU/Linux)

# Dependencies

apt-get install sox 

