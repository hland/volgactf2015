#!/bin/sh

# Run this file like this
# ./solve.sh 4
# to solve 4 captchas


# command to split wav file on silence:
# sox captcha.1 captcha1.wav silence 1 0.10 0.1% 1 0.10 0.1% : newfile : restart

d=$(date +%Y%m%d_%H%M%S)
mkdir ${d}
cd ${d}
pwd
# Get a cookie
wget --keep-session-cookies --save-cookies cookies.txt http://yacst.2015.volgactf.ru -O "get_cookie.html";

# Pull down 5 captchas:
for i in `seq 1 $1`; 
do 
    wget --load-cookies cookies.txt http://yacst.2015.volgactf.ru/captcha -O "captcha$i";
    cat cookies.txt
    sox "captcha$i" "captcha$i.wav" silence 1 0.10 0.1% 1 0.15 0.1% : newfile : restart

    python ../solve.py `pwd` $i
done

# Split all:
for i in `seq 1 $1`; do sox "captcha$i" "captcha$i.wav" silence 1 0.10 0.1% 1 0.15 0.1% : newfile : restart; done

#for i in `seq 1 6`;
#do
#    for j in `seq 1 5`;
#    do
#        ls -la "captcha${i}00${j}.wav"
#    done;
#done;
